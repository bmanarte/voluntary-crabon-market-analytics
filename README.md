# Voluntary Crabon Market Analytics

## Getting started

This GitLab showcases the work developed for a **Field Lab Work Project**. The work assessed a framework of tasks given by Celfocus with the objective to help decison-mikng by giving informed insights into Voluntary Carbon Market(VCM).


![drawio.png](./drawio.png)


The diagram above breaks down what was done for each part of the project. Starting with a general view of the companys' tasks, the diagram goes down representing the thought process of the authors in each step of the work project. Passing through litrature review, data and analysis, that can be found in this GitLab.

The corresponding notebooks for the analysis are as follows:

- Exploratory data analysis - ![1_Global_analysis](./1_Global_analysis.ipynb)
- Carbon Pricing - ![2_Certifier_Calculator](./2_Certifier_Calculator.ipynb)
- Certification Cost - ![3_Abatable&Quantum](./3_Abatable&Quantum.ipynb)
- Europe VCM Ranking - ![4_VCM_adoption index](./4_VCM_adoption index.ipynb)

# Authors

- Bernardo Manarte - 55810 - 55810@novasbe.pt

- Francisco Bentes - 55566 - 55566@novasbe.pt


# License
This project is licensed under the GNU General Public License v3.0. (GNU)
