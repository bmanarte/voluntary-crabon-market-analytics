class VCSCertificationFeesCalculator:
    def __init__(self):
        # Initialize fee rates
        self.account_opening_fee = 500
        self.account_maintenance_fee = 500
        self.pipeline_listing_request_fee = 1000
        self.project_registration_request_review_fee = 2500
        self.vcu_issuance_levy = 0.20
        self.methodology_review_fee_initial_submission = 2000
        self.methodology_review_fee_draft_submission = 13000
        self.module_tool_review_fee_initial_submission = 1500
        self.module_tool_review_fee_draft_submission = 6000
        self.minor_revision_review_fee = 6000
        self.methodology_compensation_rebate = {
            1000000: 0.02,
            2000000: 0.018,
            4000000: 0.016,
            6000000: 0.012,
            8000000: 0.008,
            10000000: 0.004,
            60000000: 0.002
        }
       #VVB fees from gold
        self.vvb_application_review_fee_3y = 2500
        self.vvb_application_renewal_fee_3y= 1500
        self.vvb_application_approval_fee_y = 1500

    
    def calculate_account_opening_fee(self):      #y
        return self.account_opening_fee
    
    def calculate_account_maintenance_fee(self, years):      #y
        return self.account_maintenance_fee * years
    
    def calculate_pipeline_listing_request_fee(self):      #y
        return self.pipeline_listing_request_fee
    
    def calculate_project_registration_request_review_fee(self):      #y
        return self.project_registration_request_review_fee
    
    def calculate_vcu_issuance_fee(self, num_vcus_issued):      #y
        return self.vcu_issuance_levy * num_vcus_issued
    
    def calculate_methodology_review_fee(self, initial_submission=True):      #n
        if initial_submission:
            return self.methodology_review_fee_initial_submission
        else:
            return self.methodology_review_fee_draft_submission
    
    def calculate_module_tool_review_fee(self, initial_submission=True):      #n
        if initial_submission:
            return self.module_tool_review_fee_initial_submission
        else:
            return self.module_tool_review_fee_draft_submission
    
    def calculate_minor_revision_review_fee(self):      #n
        return self.minor_revision_review_fee
    
    def calculate_methodology_compensation_rebate(self, num_vcus_issued):      #n
        rebate = 0
        for volume, rate in self.methodology_compensation_rebate.items():
            if num_vcus_issued <= volume:
                rebate = rate * num_vcus_issued
                break
        return rebate
    
    def calculate_vvb_fee_application_review(self):
        a = self.vvb_application_review_fee_3y
        return a
    
    def calculate_vvb_fee_application_renewal(self):
        return self.vvb_application_renewal_fee_3y
    
    def calculate_vvb_fee_application_approval(self):
        return self.vvb_application_approval_fee_y
    
   


    
