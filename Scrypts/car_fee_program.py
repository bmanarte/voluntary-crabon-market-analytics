#CAR certifier costs
class CarCertificationFeesCalculator:
    def __init__(self):
        self.issuance_fee = 0.19
        self.vvb_application_review_fee_3y = 2500
        self.vvb_application_renewal_fee_3y= 1500
        self.vvb_application_approval_fee_y = 1500
        self.transfer_fee_per_credit = 0.03
        self.cancellation_fee_per_credit = 0.03
        self.methodology_review_fees = 13500

    #Gold Standard VVB fees
    def calculate_vvb_fee_application_review(self):
        return self.vvb_application_review_fee_3y
        
    def calculate_vvb_fee_application_renewal(self):
        return self.vvb_application_renewal_fee_3y
    
    def calculate_vvb_fee_application_approval(self):
        return self.vvb_application_approval_fee_y
    
    def methodology_review_fee(self):
        return self.methodology_review_fees
    
    def account_setup_fee(self):
        return 500
    
    def account_reactivation_fee(self):
        return 500
    
    def account_maintenance_fee(self):
        return 500
    
    def project_owner_setup_fee(self):
        return 200
    
    def project_owner_maintenance_fee(self):
        return 200
    
    def project_submittal_fee_arb_protocol(self):
        return 700
    
    def project_submittal_fee_reserve_protocol(self):
        return 500
    
    def project_variance_review_fee(self):
        return 1350
    
    def calculate_issuance_fee(self, num_vcus_issued):
        return self.issuance_fee * num_vcus_issued
    
    def transfer_fee_transferor(self, tonne_co2e):
        return tonne_co2e * self.transfer_fee_per_credit
    
    def cancellation_fee(self, credits):
        return 0.03*credits
    
    def retirement_fee(self):
        return 0  # No charge
    
    def project_transfer_fee_transferee(self):
        return 500
    
    def late_payment_fee(self, amount):
        # Implement logic to calculate late payment fee based on the given amount
        # This can be a variable fee depending on the late payment amount
        # For example, you can calculate a percentage of the overdue amount as the fee
        # For this example, I'll return a fixed late payment fee of $50.
        return 50
    
    
