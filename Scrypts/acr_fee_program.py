class ACRFeeCalculator:
    def __init__(self):
        self.account_opening_fee = 500
        self.account_opening_custodial_fee = 10000
        self.project_listing_review_fee = 1000
        self.project_transfer_fee_per_tonne_co2e = 0.08
        self.project_validation_review_fee = 5000
        self.second_verification_review_fee = 1500
        self.activation_fee_per_credit = 0.15
        self.transfer_fee_per_credit = 0.02
        self.cancellation_fee_per_credit = 0.03
        self.retirement_fee_per_credit = 0.02
        self.concept_note_review_fee = 5000
        self.proposed_methodology_review_minor_modifications_fee = 7500
        self.proposed_methodology_review_new_modifications_fee = 12000
        #same vvb fees as Gold Standard
        self.vvb_application_review_fee_3y = 2500
        self.vvb_application_renewal_fee_3y= 1500
        self.vvb_application_approval_fee_y = 1500

    def calculate_account_opening_fee(self):                                   #y
        return self.account_opening_fee

    def calculate_account_opening_custodial_fee(self):                         #TBD
        return self.account_opening_custodial_fee

    def calculate_validation_verification_body_application_fee(self):          #n
        return self.validation_verification_body_application_fee

    def calculate_project_listing_review_fee(self):                            #y
        return self.project_listing_review_fee

    def calculate_project_transfer_fee(self, tonne_co2e):                      #n
        return tonne_co2e * self.project_transfer_fee_per_tonne_co2e

    def calculate_project_validation_review_fee(self):                         #y
        return self.project_validation_review_fee

    def calculate_second_verification_review_fee(self):                        #y
        return self.second_verification_review_fee

    def calculate_activation_fee(self, credits):                               #y
        return self.activation_fee_per_credit * credits
  
    def calculate_transfer_fee(self):                                          #n
        return self.transfer_fee_per_credit

    def calculate_cancellation_fee(self, credits):                             #y
        return self.cancellation_fee_per_credit* credits

    def calculate_retirement_fee(self, credits):                               #y
        return self.retirement_fee_per_credit* credits

    def calculate_concept_note_review_fee(self):                               #y
        return self.concept_note_review_fee

    def calculate_proposed_methodology_review_minor_modifications_fee(self):   #n
        return self.proposed_methodology_review_minor_modifications_fee

    def calculate_proposed_methodology_review_new_modifications_fee(self):     #n
        return self.proposed_methodology_review_new_modifications_fee
    
    def calculate_vvb_fee_application_review(self):
        a = self.vvb_application_review_fee_3y
        return a
    
    def calculate_vvb_fee_application_renewal(self):
        return self.vvb_application_renewal_fee_3y
    
    def calculate_vvb_fee_application_approval(self):
        return self.vvb_application_approval_fee_y
