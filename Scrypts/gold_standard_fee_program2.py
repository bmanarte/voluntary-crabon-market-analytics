#GS certifier costs
class GSCertificationFeesCalculator:
    def __init__(self):
        # Initialize account fee rates
        self.annual_registry_account_fee = 1000
        self.account_reactivation_fee = 2500 #per suspension
        
        #VVB fees
        self.vvb_application_review_fee_3y = 2500
        self.vvb_application_renewal_fee_3y= 1500
        self.vvb_application_approval_fee_y = 1500

        #
        self.methodology_review_fee = 13500
    

        #Time of issuance ( assume fee model Cash)
        self.firstyear = 0.3
        self.followingyears = 0.30

        #self.credit_renewal_period = 0.15

    def calculate_methodology_review_fee(self):
        return self.methodology_review_fee
    def calculate_annual_registry_account_fee(self):
        return self.annual_registry_account_fee
    
    def calculate_account_reactivation_fee(self):
        return self.account_reactivation_fee

    def calculate_project_design_review(self, num_vers_issued):
        return self.pdr * num_vers_issued - 900
    
    def calculate_first_year_issuance(self, num_vers_issued_y1):
        return self.firstyear * num_vers_issued_y1 - 1000
    
    def calculate_following_years_issuance(self, num_vers_issued):
        if (self.followingyears * num_vers_issued - 2000) > 0:
            return self.followingyears * num_vers_issued - 2000
        else:
            return 2000
    
    #def calculate_credit_renewal_period(self, num_vers_issued):
        #return self.credit_renewal_period * num_vers_issued - 1000
    
    def calculate_vvb_fee_application_review(self):
        a = self.vvb_application_review_fee_3y
        return a
    
    def calculate_vvb_fee_application_renewal(self):
        return self.vvb_application_renewal_fee_3y
    
    def calculate_vvb_fee_application_approval(self):
        return self.vvb_application_approval_fee_y
    
